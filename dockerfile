# FROM continuumio/miniconda3:4.12.0
# RUN conda install -y -c conda-forge mamba
# RUN mamba create -n picrust2 -y -c bioconda -c conda-forge picrust2
# docker commit to build docker image

FROM registry.gitlab.com/sirius1018/picrust_2:1
RUN echo "source activate picrust2" > ~/.bashrc
# ENV PATH /opt/conda/envs/picrust2/bin/:$PATH
RUN /bin/bash -c "source activate picrust2"